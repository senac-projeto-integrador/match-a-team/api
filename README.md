<h1> Match a Team </h1>

Os comandos abaixo devem ser executados antes do script.

bcdedit /set hypervisorlaunchtype off

docker run -p 3306:3306 --name mysql-mariadb -e MYSQL_ROOT_PASSWORD=root -d mariadb

Abaixo segue o script do banco de dados.



# Api

bcdedit /set hypervisorlaunchtype off

docker run -p 3306:3306 --name mysql-mariadb -e MYSQL_ROOT_PASSWORD=root -d mariadb

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;

USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gener` TINYINT NOT NULL,
  `type` TINYINT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `is_active` TINYINT NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `date_birth` DATE NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `cpf` CHAR(11) NOT NULL,
  `score` DECIMAL(2,1) NOT NULL,
  `picture` LONGTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`gyms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`gyms` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `users_id` INT NOT NULL,
  `is_active` TINYINT NOT NULL,
  `score` DECIMAL(2,1) NOT NULL,
  `initial_function_time` DATETIME NOT NULL,
  `end_function_time` DATETIME NOT NULL,
  `picture` LONGTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Ginásio_Usuário1_idx` (`users_id` ASC),
  CONSTRAINT `fk_Ginásio_Usuário1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`schedules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`schedules` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `created_by` ENUM('Usuário', 'Proprietário') NOT NULL,
  `type` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Schedule_Usuário1_idx` (`users_id` ASC),
  CONSTRAINT `fk_Schedule_Usuário1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`courts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`courts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `member_max` INT NOT NULL,
  `member_min` INT NOT NULL,
  `gyms_id` INT NOT NULL,
  `schedules_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Quadra_Ginásio1_idx` (`gyms_id` ASC),
  INDEX `fk_courts_schedules1_idx` (`schedules_id` ASC),
  CONSTRAINT `fk_Quadra_Ginásio1`
    FOREIGN KEY (`gyms_id`)
    REFERENCES `mydb`.`gyms` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_courts_schedules1`
    FOREIGN KEY (`schedules_id`)
    REFERENCES `mydb`.`schedules` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`adresses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`adresses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cep` CHAR(8) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `number` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_adresses_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_adresses_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Matchs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Matchs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gyms_id` INT NOT NULL,
  `courts_id` INT NOT NULL,
  `start_time` VARCHAR(45) NOT NULL,
  `end_time` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `type` TINYINT NOT NULL,
  `description` VARCHAR(45) NULL,
  `responsible_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Matchs_gyms1_idx` (`gyms_id` ASC),
  INDEX `fk_Matchs_courts1_idx` (`courts_id` ASC),
  INDEX `fk_Matchs_users1_idx` (`responsible_id` ASC),
  CONSTRAINT `fk_Matchs_gyms1`
    FOREIGN KEY (`gyms_id`)
    REFERENCES `mydb`.`gyms` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Matchs_courts1`
    FOREIGN KEY (`courts_id`)
    REFERENCES `mydb`.`courts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Matchs_users1`
    FOREIGN KEY (`responsible_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Matchs_has_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Matchs_has_users` (
  `Matchs_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`Matchs_id`, `users_id`),
  INDEX `fk_Matchs_has_users_users1_idx` (`users_id` ASC),
  INDEX `fk_Matchs_has_users_Matchs1_idx` (`Matchs_id` ASC),
  CONSTRAINT `fk_Matchs_has_users_Matchs1`
    FOREIGN KEY (`Matchs_id`)
    REFERENCES `mydb`.`Matchs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Matchs_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




Logo após rodar o script, é necessário usar alguns comandos (através do cmd):

npm install

npm start

Depois de executar os comandos, a API já estará funcionando.


<h1>Heroku</h1>

Caso preferir, pode acessar o link do Heroku: 

https://matchateam.herokuapp.com/



