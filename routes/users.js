const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const multer = require('multer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = require('../middleware/login');


const upload = multer({ dest: 'uploads/' });
//teste
// retorna
router.get('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM users;',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// inseri
router.post('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM users WHERE email = ?',
            [req.body.email],
            (error, resultado, field) => {
                if (error) { return res.status(500).send({ error: error }) }
                if (resultado.length > 0) {
                    conn.release();
                    res.status(401).send({ mensagem: 'Usuario já cadastrado' })
                } else {
                    bcrypt.hash(req.body.password, 10, (errBcrypt, hash) => {
                        if (errBcrypt) { return res.status(500).send({ error: errBcrypt }) }
                        conn.query(
                            'INSERT INTO users (gener, type, name, is_active, username, password, date_birth, phone, email, cpf, score) VALUES (?,?,?,?,?,?,?,?,?,?,?)',
                            [req.body.gener, req.body.type, req.body.name, req.body.is_active, req.body.username, hash, req.body.date_birth, req.body.phone, req.body.email, req.body.cpf, req.body.score],
                            (error, resultado, field) => {
                                conn.release();
                                if (error) { return res.status(500).send({ error: error }) }
                                res.status(201).send({
                                    mensagem: 'Usuario cadastrado com sucesso',
                                    id: resultado.insertId,
                                });
                            }
                        )
                    })
                }
            });
    });
});

router.post('/login/', (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM users WHERE email = ?',
            [req.body.email],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                if (resultado.length < 1) {
                    return res.status(401).send({mensagem: "Falha na autenticação" })
                }
                bcrypt.compare(req.body.password, resultado[0].password, (err, result) => {
                   
                    if (err){
                        console.log(err)
                        return res.status(401).send({mensagem: "Falha na autenticação" })
                    }
                    if (result){
                        let token = jwt.sign({
                            id_users: resultado[0].id,
                            email: resultado[0].email
                        }, 'segredo',
                        {
                            expiresIn: "24h"
                        });
                        
                        return res.status(200).send({
                            mensagem: 'autenticado com sucesso',
                            token: token,
                            id: resultado[0].id,
                            type: resultado[0].type
                        });
                    }
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM users WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});
// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE users SET gener = ?, type = ?, name = ?, is_active = ?, username = ?, password = ?, date_birth = ?, phone = ?, email = ?, cpf = ?, score = ? WHERE id = ?',
            [req.body.gener, req.body.type, req.body.name, req.body.is_active, req.body.username, req.body.password, req.body.date_birth, req.body.phone, req.body.email, req.body.cpf, req.body.score, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'Usuario alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

// update em foto
router.put('/userPicture/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE users SET picture = ? WHERE id = ?',
            [req.file.picture, req.params.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'Picture alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});



module.exports = router;